import Vue from 'vue'
import VueRouter from 'vue-router'
/* Layout */
import Layout from '@/layout';
import LoginLayout from '@/layout/login';


Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/index'),
        name: 'Home',
        meta: { title: 'Home', icon: 'home', noCache: false },
      },
    ],
  },
  {
    path: '',
    component: Layout,
    redirect: '/',
    children: [
      {
        path: '/',
        component: () => import('@/views/home/index'),
        name: 'Home',
        meta: { title: 'Home', icon: 'home', noCache: false },
      },
    ],
  },
  {
    path: '',
    component: Layout,
    redirect: '/upload-products',
    children: [
      {
        path: '/upload-products',
        component: () => import('@/views/home/upload'),
        name: 'Upload',
        meta: { title: 'Upload', icon: 'upload', noCache: false },
      },
    ],
  },

  {
    path: '',
    component: LoginLayout,
    redirect: 'register',
    children: [
      {
        path: 'register',
        component: () => import('@/views/auth/register'),
        name: 'Register',
        meta: { title: 'Register', icon: 'Register', noCache: false },
      },
    ],
  },

  {
    path: '',
    component: Layout,
    redirect: '404',
    children: [
      {
        path: '404',
        component: () => import('@/views/common/404'),
        name: '404',
        meta: { title: '404', icon: '404', noCache: false },
      },
    ],
  },

  {
    path: '',
    component: Layout,
    redirect: '403',
    children: [
      {
        path: '403',
        component: () => import('@/views/common/403'),
        name: 'Email Not Verified',
        meta: { title: 'Email Not Verified', icon: 'Email Not Verified', noCache: false },
      },
    ],
  },

  //  Login Layout

  {
    path: '',
    component: LoginLayout,
    redirect: 'login',
    children: [
      {
        path: 'login',
        component: () => import('@/views/auth/login'),
        name: 'Login',
        meta: { title: 'Login', icon: 'Login', noCache: false },
      },
    ],
  },

]

const createRouter = () => new VueRouter({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  base: process.env.MIX_LARAVUE_PATH,
  routes: routes,
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}


export default router;

