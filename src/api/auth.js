import request from '@/utils/request';

export function login(query) {
  return request({
    url: '/login',
    method: 'post',
    data:query,
  });

}
export function register(query) {
  return request({
    url: '/register',
    method: 'post',
    data:query,
  });
}

export function forgotPasswordEmail(query) {
  return request({
    url: '/forgot-password',
    method: 'post',
    data:query,
  });
}

export function forgotPasswordPhone(query) {
  return request({
    url: '/send-otp-to-phone',
    method: 'post',
    data:query,
  });
}

export function passwordResetOtpVerify(query) {
  return request({
    url: '/verify-otp-and-password-reset',
    method: 'post',
    data:query,
  });
}

export function passwordResetConfirmPassword(query) {
  return request({
    url: '/reset-password-with-phone-token',
    method: 'post',
    data:query,
  });
}




export function loginOtp(query) {
  return request({
    url: '/login-otp',
    method: 'post',
    data:query,
  });

}

export function loginConfirm(query) {
  return request({
    url: '/verify-otp',
    method: 'post',
    data:query,
  });
}

export function confirmForgotPassword(query) {
  return request({
    url: '/reset-password-with-token',
    method: 'post',
    data:query,
  });
}

export function resentVerificationMail() {
  return request({
    url: '/user/resent-email-verification',
    method: 'post',
    data:{},
  });
}

export function resetPassword(query) {
  return request({
    url: '/reset-password ',
    method: 'post',
    data:query,
  });
}

export function profile(query) {
  return request({
    url: '/profile',
    method: 'post',
    data:query,
  });
}

export function updateProfile(query) {
  return request({
    url: '/update-profile',
    method: 'post',
    data:query,
  });
}

export function sendOtp() {
  return request({
    url: '/generate-otp',
    method: 'post',
    data:{},
  });
}



