import request from '@/utils/request';

export function uploadProducts(query) {
  return request({
    url: '/products/upload',
    method: 'post',
    data:query,
  });
}


export function fetcProducts(query,header) {
  return request({
    url: '/products',
    method: 'get',
    header: header,
    data:query,
  });
}







